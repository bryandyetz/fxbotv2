<?php
session_start();
include('../php-includes/check-login.php');
require('../php-includes/connect.php');

$userid = $_SESSION['userid'];
if( !empty($userid) ){
  $getAddrData = "select deposit_address,balance from users where username='$userid' ";
  $queryAddrData = mysqli_query($con, $getAddrData);
  $fetchAddrData = mysqli_fetch_assoc($queryAddrData);
  $payTo = $fetchAddrData["deposit_address"];
  $balance = $fetchAddrData["balance"];
  $balance = $balance / 100000000;
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Cash in - FXT v2</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="../assets/images/favicon.ico">

        <link rel="stylesheet" href="../plugins/morris/morris.css">

        <!-- App css -->
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />

        <script src="../assets/js/modernizr.min.js"></script>

    </head>

    <body>

        <!-- Navigation Bar-->
        <?php include('../php-includes/nav_bar.php'); ?>
        <!-- End Navigation Bar-->


        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="btn-group pull-right">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">FXT v2</a></li>
                                    <li class="breadcrumb-item active">Cash in</li>
                                </ol>
                            </div>
                            <!-- <h4 class="page-title">Welcome !</h4> -->
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->

                <div class="row">
                  <br />
                </div>

                <div class="row">
                  <!-- /filler -->
                    <div class="col-md-2"><p>&nbsp;</p></div>
                  <!-- /filler -->

                    <div class="col-md-8">
                        <div>
                          <div class="card-box">
                              <h4 class="text-dark  header-title m-t-0 m-b-30">Cash in</h4>

                              <span class="text-custom">Wallet Balance <span class="text-info" style="font-weight: bold; font-size: 105%">0.02500000 BTC</span></span>&nbsp;&nbsp;&nbsp;&nbsp;
                              <button type="button" class="btn btn-primary btn-custom waves-effect w-lg waves-light m-b-5" data-toggle="modal" data-target="#rvnDiv">DEPOSIT</button>

                              <table class="table table-responsive">
                                <thead>
                                  <tr>
                                    <th>Date Received</th>
                                    <th>Amount</th>
                                    <th>Status</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <?php
                                    $getDepHist = "select date_format(created_at,'%m/%d/%Y') as dt_rec,deposit_address,amount,status from deposits where deposit_address = '$payTo' order by created_at desc ";
                                    $queryDepHist = mysqli_query($con, $getDepHist);
                                      while($fetchDepHist = mysqli_fetch_array($queryDepHist)){
                                        $dateReceived = $fetchDepHist['dt_rec'];
                                        $amount = $fetchDepHist['amount'];
                                          $amount = $amount / 100000000;
                                        $status = $fetchDepHist['status'];
                                  ?>
                                  <tr>
                                    <td><?php echo $dateReceived; ?></td>
                                    <td><?php echo number_format($amount,8); ?></td>
                                    <td><?php echo $status; ?></td>
                                  </tr>
                                  <?php } ?>
                                </tbody>
                              </table>
                              <!-- Modal -->
                              <div class="modal fade" id="rvnDiv" role="dialog">
                                <div class="modal-dialog">

                                  <!-- Modal content-->
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                      <div class="text-center">
                                        <img src="http://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=<?php echo $payTo; ?>" ></img><br>
                                      </div>
                                      <span class="text-dark"><strong>IMPORTANT:</strong> Send only <strong>BTC</strong> to this deposit address. Sending any other currency to this address may result in the loss of your deposit</span>
                                      <p>&nbsp;</p>
                                      <div class="form-group">
                                        <label for="btcAddr">BTC address</label>
                                        <input type="text" class="form-control text-center" id="btcAddr" name="btcAddr" placeholder="0" value="<?php echo $payTo; ?>" readonly>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-default waves-effect waves-light m-b-5" data-dismiss="modal">Close</button>
                                      </div>
                                    </div>
                                  </div>

                                </div>
                              </div>
                          </div><!-- /end card box -->
                        </div>
                    </div>

                    <!-- /filler -->
                      <div class="col-md-2"><p>&nbsp;</p></div>
                    <!-- /filler -->
                </div>

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->


        <!-- Footer -->
        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        2019 © FXT v2
                    </div>
                </div>
            </div>
        </footer>
        <!-- End Footer -->


        <!-- jQuery  -->
        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/popper.min.js"></script><!-- Popper for Bootstrap --><!-- Tether for Bootstrap -->
        <script src="../assets/js/bootstrap.min.js"></script>
        <script src="../assets/js/waves.js"></script>
        <script src="../assets/js/jquery.slimscroll.js"></script>
        <script src="../assets/js/jquery.scrollTo.min.js"></script>

        <!-- Counter Up  -->
        <script src="../plugins/waypoints/lib/jquery.waypoints.min.js"></script>
        <script src="../plugins/counterup/jquery.counterup.min.js"></script>

        <!--Morris Chart-->
        <script src="../plugins/morris/morris.min.js"></script>
        <script src="../plugins/raphael/raphael-min.js"></script>

        <!-- Page js  -->
        <script src="../assets/pages/jquery.dashboard.js"></script>

        <!-- Custom main Js -->
        <script src="../assets/js/jquery.core.js"></script>
        <script src="../assets/js/jquery.app.js"></script>


        <script type="text/javascript">
            jQuery(document).ready(function($) {
                $('.counter').counterUp({
                    delay: 100,
                    time: 1200
                });
            });
        </script>

    </body>
</html>
