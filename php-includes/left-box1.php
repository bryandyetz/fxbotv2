<div class="card-box">
    <!-- <h4 class="text-dark  header-title m-t-0 m-b-30">Total Revenue</h4> -->

    <ul class="nav nav-tabs tabs-bordered">
        <li class="nav-item">
            <a href="#php" data-toggle="tab" aria-expanded="true" class="nav-link active">
                PHP
            </a>
        </li>
        <li class="nav-item">
            <a href="#btc" data-toggle="tab" aria-expanded="false" class="nav-link">
                BTC
            </a>
        </li>
    </ul>

    <div class="tab-content">
      <div class="tab-pane fade active show" id="php" aria-expanded="true">
          <h5>PHP 320.00</h5>
          <p>&nbsp;</p>
      </div>
      <div class="tab-pane fade" id="btc" aria-expanded="false">
          <h5>BTC 0.0023</h5>
          <button type="button" class="btn btn-default btn-custom waves-effect w-md m-b-5">Receive</button>
      </div>
  </div>
</div>
