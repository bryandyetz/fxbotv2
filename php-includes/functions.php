<?php
function activateNullValidation($val){
  if( empty($val) || $val == ''  ){
    $errVal = 1;
  }else{
    $errVal = 0;
  }
  return $errVal;
}

function activationValidateEmail($email){
  if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    $emailErr = 1;
  }else{
    $emailErr = 0;
  }
  return $emailErr;
}

function comparePassw($pas_val,$conpas_val){
  if($pas_val != $conpas_val){
    $errValps = 1;
  }else{
    $errValps = 0;
  }
  return $errValps;
}

function random_alphanum($length){
// POSSIBLE COMBINATIONS > HUNDREDS OF MILLIONS IF LENGTH > 6
//           1...5...10...15...20...25...30.
  $alphanum = "abcdefghijklmnopqrstuvwxyzABCDEFGHJKMNOPQRSTUVWXYZ123456789";
  $strlen   = strlen($alphanum);
  $string   = NULL;
  while(strlen($string) < $length){
      $random = mt_rand(0,$strlen);
      $string .= substr($alphanum, $random, 1);
  }
  return($string);
}

function walletBalance($con,$userId){
	$userid = $userId;
	$getBalData = "select balance from users where tg_id='$userid' ";
	$queryBalData = mysqli_query($con, $getBalData);
	  $fetchBalData = mysqli_fetch_assoc($queryBalData);
	    $balanceSat = $fetchBalData["balance"];
	      $balance = $balanceSat / 100000000;

	return $balance;
}

function getDayOfWk($con){
  $sqlDayOfWk = "select dayofweek(CURDATE()) as dy_of_wk from dual";
  $queryDayOfWk = mysqli_query($con,$sqlDayOfWk);
    $fetchDayOfWk = mysqli_fetch_assoc($queryDayOfWk);
    $dyOfWk = $fetchDayOfWk['dy_of_wk'];

  return $dyOfWk;
}

function insPayoutScheds($con,$tg_id,$dy_of_wk,$usrid,$amtusd,$amtsat,$token,$robot_nm){

  //validations
  if( $dy_of_wk == 1 ){
    mysqli_query($con,"insert into payouts (tg_id,user_id,portfolio_id,po_sched,amt_usd,amt_sat,po_type,robot) values ('".$tg_id."',".$usrid.",'".$token."',DATE_ADD(CURDATE(), INTERVAL 12 DAY),'".$amtusd."','".$amtsat."','INTEREST','".$robot_nm."')");
  }elseif( $dy_of_wk == 2 ){
    mysqli_query($con,"insert into payouts (tg_id,user_id,portfolio_id,po_sched,amt_usd,amt_sat,po_type,robot) values ('".$tg_id."',".$usrid.",'".$token."',DATE_ADD(CURDATE(), INTERVAL 18 DAY),'".$amtusd."','".$amtsat."','INTEREST','".$robot_nm."')");
  }elseif( $dy_of_wk == 3 ){
    mysqli_query($con,"insert into payouts (tg_id,user_id,portfolio_id,po_sched,amt_usd,amt_sat,po_type,robot) values ('".$tg_id."',".$usrid.",'".$token."',DATE_ADD(CURDATE(), INTERVAL 17 DAY),'".$amtusd."','".$amtsat."','INTEREST','".$robot_nm."')");
  }elseif( $dy_of_wk == 4 ){
    mysqli_query($con,"insert into payouts (tg_id,user_id,portfolio_id,po_sched,amt_usd,amt_sat,po_type,robot) values ('".$tg_id."',".$usrid.",'".$token."',DATE_ADD(CURDATE(), INTERVAL 16 DAY),'".$amtusd."','".$amtsat."','INTEREST','".$robot_nm."')");
  }elseif( $dy_of_wk == 5 ){
    mysqli_query($con,"insert into payouts (tg_id,user_id,portfolio_id,po_sched,amt_usd,amt_sat,po_type,robot) values ('".$tg_id."',".$usrid.",'".$token."',DATE_ADD(CURDATE(), INTERVAL 15 DAY),'".$amtusd."','".$amtsat."','INTEREST','".$robot_nm."')");
  }elseif( $dy_of_wk == 6 ){
    mysqli_query($con,"insert into payouts (tg_id,user_id,portfolio_id,po_sched,amt_usd,amt_sat,po_type,robot) values ('".$tg_id."',".$usrid.",'".$token."',DATE_ADD(CURDATE(), INTERVAL 14 DAY),'".$amtusd."','".$amtsat."','INTEREST','".$robot_nm."')");
  }elseif( $dy_of_wk == 7 ){
    mysqli_query($con,"insert into payouts (tg_id,user_id,portfolio_id,po_sched,amt_usd,amt_sat,po_type,robot) values ('".$tg_id."',".$usrid.",'".$token."',DATE_ADD(CURDATE(), INTERVAL 13 DAY),'".$amtusd."','".$amtsat."','INTEREST','".$robot_nm."')");
  }else{
    $do_nothing = "";
  }
}

function insDrbBon2($con,$dy_of_wk,$user_id,$amtusd,$amtsat,$token){

  $queryUsr1 = "";
  $queryUsr2 = "";
  $queryUsr3 = "";
  $queryUsr4 = "";
  $queryUsr5 = "";
  $queryUsr6 = "";

  $curr_sponsor_id1 = "";
  $curr_sponsor_id2 = "";
  $curr_sponsor_id3 = "";
  $curr_sponsor_id4 = "";
  $curr_sponsor_id5 = "";
  $curr_sponsor_id6 = "";

  $sponsorID1 = "";
  $sponsorID2 = "";
  $sponsorID3 = "";
  $sponsorID4 = "";
  $sponsorID5 = "";
  $sponsorID6 = "";

  //1st loop
  $queryUsr1 = mysqli_query($con,"select sponsor_id from users where id = '$user_id'");
    $fetchUsr1 = mysqli_fetch_assoc($queryUsr1);
      $curr_sponsor_id1 = $fetchUsr1['sponsor_id'];
      $sponsorID1 = sponsorFinder($con, $curr_sponsor_id1);
      if( (!empty($sponsorID1) || $sponsorID1 != "") && $sponsorID1 != 1 ){
        drbGiver($con,$dy_of_wk,$sponsorID1,1,$amtusd,$amtsat,$token);
      }

  //2nd loop
  $queryUsr2 = mysqli_query($con,"select sponsor_id from users where id = '$sponsorID1'");
    $fetchUsr2 = mysqli_fetch_assoc($queryUsr2);
      $curr_sponsor_id2 = $fetchUsr2['sponsor_id'];
      $sponsorID2 = sponsorFinder($con, $curr_sponsor_id2);
      if( (!empty($sponsorID2) || $sponsorID2 != "") && $sponsorID2 != 1 ){
        drbGiver($con,$dy_of_wk,$sponsorID2,2,$amtusd,$amtsat,$token);
      }

  //3rd loop
  $queryUsr3 = mysqli_query($con,"select sponsor_id from users where id = '$sponsorID2'");
    $fetchUsr3 = mysqli_fetch_assoc($queryUsr3);
      $curr_sponsor_id3 = $fetchUsr3['sponsor_id'];
      $sponsorID3 = sponsorFinder($con, $curr_sponsor_id3);
      if( (!empty($sponsorID3) || $sponsorID3 != "") && $sponsorID3 != 1 ){
        drbGiver($con,$dy_of_wk,$sponsorID3,3,$amtusd,$amtsat,$token);
      }

  //4th loop
  $queryUsr4 = mysqli_query($con,"select sponsor_id from users where id = '$sponsorID3'");
    $fetchUsr4 = mysqli_fetch_assoc($queryUsr4);
      $curr_sponsor_id4 = $fetchUsr4['sponsor_id'];
      $sponsorID4 = sponsorFinder($con, $curr_sponsor_id4);
      if( (!empty($sponsorID4) || $sponsorID4 != "") && $sponsorID4 != 1 ){
        drbGiver($con,$dy_of_wk,$sponsorID4,4,$amtusd,$amtsat,$token);
      }

  //5th loop
  $queryUsr5 = mysqli_query($con,"select sponsor_id from users where id = '$sponsorID4'");
    $fetchUsr5 = mysqli_fetch_assoc($queryUsr5);
      $curr_sponsor_id5 = $fetchUsr5['sponsor_id'];
      $sponsorID5 = sponsorFinder($con, $curr_sponsor_id5);
      if( (!empty($sponsorID5) || $sponsorID5 != "") && $sponsorID5 != 1 ){
        drbGiver($con,$dy_of_wk,$sponsorID5,5,$amtusd,$amtsat,$token);
      }

  //6th loop
  $queryUsr6 = mysqli_query($con,"select sponsor_id from users where id = '$sponsorID5'");
    $fetchUsr6 = mysqli_fetch_assoc($queryUsr6);
      $curr_sponsor_id6 = $fetchUsr6['sponsor_id'];
      $sponsorID6 = sponsorFinder($con, $curr_sponsor_id6);
      if( (!empty($sponsorID6) || $sponsorID6 != "") && $sponsorID6 != 1 ){
        drbGiver($con,$dy_of_wk,$sponsorID6,6,$amtusd,$amtsat,$token);
      }

  /*for($i=1;$i<=6;$i++){

  }*/

  return $token;
}

function sponsorFinder($con, $usrID){

  $querySpr = mysqli_query($con,"select id as upline_userid from users where ref_id = '$usrID'");
    $fetchData = mysqli_fetch_assoc($querySpr);
      $sprID = $fetchData['upline_userid'];
    return $sprID;
}

function drbGiver($con,$dy_of_wk,$userid,$level,$amtusd,$amtsat,$token){

    $isActiveInvest = portfolioFinder($con,$userid);
    //if( $isActiveInvest > 0 ){
      $lvl_pct = lvlPctFinder($level);
      $drb_payout_sched = poDRBsched($dy_of_wk);
      $po_amt = $amtusd * $lvl_pct;
      mysqli_query($con, "insert into drbpayouts (portfolio_id,user_id,po_sched,pct,level,amt_usd,amt_sat,po_amt) values ('".$token."','".$userid."',".$drb_payout_sched.",'".$lvl_pct."','".$level."',".$amtusd.",'".$amtsat."','".$po_amt."')");
    //}
}

function portfolioFinder($con,$usrID){

  $getUsrID = "";
  $sqlPrtFind = "select user_id from robots where user_id = '$usrID' and remaining_weeks > 0";
  $queryPrtFind = mysqli_query($con,$sqlPrtFind);
  $recPrtFind = mysqli_num_rows($queryPrtFind);
  //$fetchPrtFind = mysqli_fetch_assoc($queryPrtFind);
    //$getUsrID = $fetchPrtFind['user_id'];

  return $recPrtFind;
}

function lvlPctFinder($lvl){
  //lvl pct
  if( $lvl == 6 ){
    $pctlvl = 0.005;
  }elseif( $lvl == 5 ){
    $pctlvl = 0.005;
  }elseif( $lvl == 4 ){
    $pctlvl = 0.005;
  }elseif( $lvl == 3 ){
    $pctlvl = 0.010;
  }elseif( $lvl == 2 ){
    $pctlvl = 0.020;
  }elseif( $lvl == 1 ){
    $pctlvl = 0.030;
  }else{
    $pctlvl = "";
  }

  return $pctlvl;
}

function poDRBsched($dy_of_wk){
  //day of week
  if( $dy_of_wk == 1 ){
    $drb_po_sched = "DATE_ADD(CURDATE(), INTERVAL 12 DAY)";
  }elseif( $dy_of_wk == 2 ){
    $drb_po_sched = "DATE_ADD(CURDATE(), INTERVAL 18 DAY)";
  }elseif( $dy_of_wk == 3 ){
    $drb_po_sched = "DATE_ADD(CURDATE(), INTERVAL 17 DAY)";
  }elseif( $dy_of_wk == 4 ){
    $drb_po_sched = "DATE_ADD(CURDATE(), INTERVAL 16 DAY)";
  }elseif( $dy_of_wk == 5 ){
    $drb_po_sched = "DATE_ADD(CURDATE(), INTERVAL 15 DAY)";
  }elseif( $dy_of_wk == 6 ){
    $drb_po_sched = "DATE_ADD(CURDATE(), INTERVAL 14 DAY)";
  }elseif( $dy_of_wk == 7 ){
    $drb_po_sched = "DATE_ADD(CURDATE(), INTERVAL 13 DAY)";
  }else{
    $drb_po_sched = "";
  }

  return $drb_po_sched;
}

function convertToBTC($amt_sat){
  $balance = $amt_sat / 100000000; //balance in btc

  return $balance;
}

function userInfo($con,$tg_id,$col){
  $checkAddy = mysqli_query($con, "select $col from users where tg_id = '$tg_id'");
  $fetchData = mysqli_fetch_assoc($checkAddy);
    $user_info_val = $fetchData[$col];
  return $user_info_val;
}

function fx_invest($con,$tg_id,$btc_php,$botPricePhp,$botPriceUsd,$max_bot,$robotName){
  $fxInvMsg = "";
  $hiredBots = countRobots($con,$tg_id,$robotName);

  //conversions
  $botPriceBtc = $botPricePhp / $btc_php; //convert bot price into btc
	$botPriceBtc = round($botPriceBtc,8);
	$botPriceSat = $botPriceBtc * 100000000;

  $token = md5(uniqid(mt_rand(),true));
  //user info
  $userTgId = userInfo($con,$tg_id,"tg_id");
  $userId = userInfo($con,$tg_id,"id");
  $balanceSat = userInfo($con,$tg_id,"balance");
    $balance = convertToBTC($balanceSat); //balance in btc
  $sponsorID = userInfo($con,$tg_id,"sponsor_id");

  $errMinBlk = 0;
  $justError = 0;
  $errFunds = 0;
  $balPhpPrice = $btc_php * $balance;
  $balPhpPriceRnd = round($balPhpPrice,2);

  if( $balPhpPrice < $botPricePhp ){ $errFunds = 1; $fxInvMsg = "Insufficient fund!";}
  if( empty($balPhpPrice) ){ $justError = 1; $fxInvMsg = "Balance is empty!";}

  if(
  	$errFunds == 0 &&
  	$justError == 0 &&
  	!empty($token)
  ){
    if($hiredBots < $max_bot){
      //ins robot
      $queryPort = mysqli_query($con,"insert into robots ( tg_id,user_id,portfolio_id,amt_usd,amt_sat,robot ) values ( '$userTgId','$userId','$token','$botPriceUsd','$botPriceSat','$robotName' ) ");
    }else{
      $fxInvMsg = "You reached the maximum allowed robot.";
    }


  	if( $queryPort ){
  		//update balance
  		$updateBalance = "update users set balance = balance - '$botPriceSat' where tg_id = '$tg_id'";
  		$doUpdateBalance = mysqli_query($con, $updateBalance);

  		$dayOfWeek = getDayOfWk($con);

  		//ins 1st payout sched $userId
      insPayoutScheds($con,$userTgId,$dayOfWeek,$userId,$botPriceUsd,$botPriceSat,$token,$robotName);

  		//ins drb bonuses (6 levels)
      insDrbBon2($con,$dayOfWeek,$userId,$botPriceUsd,$botPriceSat,$token);

      $fxInvMsg = "Congratulations! Your trading bot is now active!";

  	}
  }

  $return_msg = $fxInvMsg;
  //$return_msg .= "<br />Hired robot: ".$hiredBots;
  /*$return_msg = "bal sat: ".$balanceSat."<br />".
                "bal btc: ".number_format($balance, 4)."<br />".
                "bal usd: ".$balPhpPrice .
                "<br />======<br />" .
                "bot price USD: ".$botPriceUsd."<br />" .
                "bot price SAT: ".$botPriceSat."<br />" .
                "errFunds: ".$errFunds."<br />" .
                "justError: ".$justError."<br />" .
                "day of week: ".$dayOfWeek."<br />".
                "hired robots: ".$hiredBots;*/

  return $return_msg;
}

function countRobots($con,$tg_id,$robotName){
  $sqlRoboFind = "select count(portfolio_id) as hired_bot from robots where tg_id = '$tg_id' and robot = '$robotName'";
  $queryRoboFind  = mysqli_query($con,$sqlRoboFind);
  $roboFind = mysqli_fetch_assoc($queryRoboFind);
    $getCount = $roboFind['hired_bot'];
  //$recRoboFind = mysqli_num_rows($queryRoboFind);
  return $getCount;
}

function fx_teams($con,$tg_id){
  require_once('language.php');
  $user_acctsusr = array();
  $user_acctsusr2 = array();
  $user_acctsusr3 = array();
  $user_acctsusr4 = array();
  $user_acctsusr5 = array();
  $user_acctsusr6 = array();
  $recLvl2 = 0;
  $recLvl3= 0;
  $recLvl4= 0;
  $recLvl5= 0;
  $recLvl6= 0;
  $acctRefId = userInfo($con,$tg_id,"ref_id");

  //echo "<br>My Referral ID: ".$acctRefId."<br>";

  //Team Level 1
  $sqlLvl1 = "select tg_id,username,ref_id from users where sponsor_id = '$acctRefId' order by created_at desc";
  $queryLvl1 = mysqli_query($con, $sqlLvl1);
  $rec = mysqli_num_rows($queryLvl1);
  //echo "<br>recLv1: ".$rec." | user_acct: ".$acctRefId."<br>";
  while( $fetchL1 = mysqli_fetch_array($queryLvl1) ){
    $userid_lv1 = $fetchL1['tg_id'];
    $name_lv1 = $fetchL1['username'];
    $acct_usern_lv1 = $fetchL1['ref_id'];
    //echo "lvl1=> tg_id: ".$userid_lv1." | ref_id ".$acct_usern_lv1." | sponsor Id: ".$acctRefId."<br>";
      $user_acctsusr[] = $acct_usern_lv1;
        $sqlCommL1 = "select sum(amt_usd) as total_usd from robots where tg_id = '$userid_lv1' and profit_usd < amt_usd group by tg_id";
        $queryCommL1 = mysqli_query($con, $sqlCommL1);
        while( $fetchCom1 = mysqli_fetch_array($queryCommL1) ){
          $amtUsd_lv1 += $fetchCom1['total_usd'];
        }
  }

  //Team Level 2
  foreach( $user_acctsusr as $key => $user_accts2 ){
    $sqlLvl2 = "select tg_id,username,ref_id from users where sponsor_id = '$user_accts2' order by created_at desc";
    $queryLvl2 = mysqli_query($con, $sqlLvl2);

    while( $fetchLvl2 = mysqli_fetch_array($queryLvl2) ){
      //$recLvl2 = mysqli_num_rows($queryLvl2);
      $recLvl2++;
      //echo "<br>recLvl2: ".$recLvl2." | user_acct: ".$user_accts2."<br>";
      $userid_lv2 = $fetchLvl2['tg_id'];
      $name_lv2 = $fetchLvl2['username'];
      $acct_usern_lvl2 = $fetchLvl2['ref_id'];
      //echo "<br>lvl2=> tg_id: ".$userid_lv2." | ref_id ".$acct_usern_lvl2." | sponsor Id: ".$user_accts2."<br>";
        $user_acctsusr2[] = $acct_usern_lvl2;
          //total active invest
          $sqlCommL2 = "select sum(amt_usd) as total_usd from robots where tg_id = '$userid_lv2' and profit_usd < amt_usd group by user_id";
          $queryCommL2 = mysqli_query($con, $sqlCommL2);
          while( $fetchCom2 = mysqli_fetch_array($queryCommL2) ){;
            $amtUsd_lv2 += $fetchCom2['total_usd'];
          }
    }
  }

  //Team Level 3
  foreach( $user_acctsusr2 as $key => $user_accts3 ){
    $sqlLvl3 = "select tg_id,username,ref_id from users where sponsor_id = '$user_accts3' order by created_at desc";
    $queryLvl3 = mysqli_query($con, $sqlLvl3);

    while( $fetchLvl3 = mysqli_fetch_array($queryLvl3) ){
      //$recLvl3 = mysqli_num_rows($queryLvl3);
      $recLvl3++;
      //echo "<br>recLvl3: ".$recLvl3." | user_acct: ".$user_accts3."<br>";
        $userid_lv3 = $fetchLvl3['tg_id'];
        $name_lv3 = $fetchLvl3['username'];
        $acct_usern_lvl3 = $fetchLvl3['ref_id'];
        //echo "<br>lvl2=> tg_id: ".$userid_lv2." | ref_id ".$acct_usern_lvl2." | sponsor Id: ".$user_accts2."<br>";
          $user_acctsusr3[] = $acct_usern_lvl3;
            //total active invest
            $sqlCommL3 = "select sum(amt_usd) as total_usd from robots where tg_id = '$userid_lv3' and profit_usd < amt_usd group by user_id";
            $queryCommL3 = mysqli_query($con, $sqlCommL3);
              while( $fetchCom3 = mysqli_fetch_array($queryCommL3) ){;
                $amtUsd_lv3 += $fetchCom3['total_usd'];
              }
    }
  }

  //Team Level 4
  foreach( $user_acctsusr3 as $key => $user_accts4 ){
    $sqlLvl4 = "select tg_id,username,ref_id from users where sponsor_id = '$user_accts4' order by created_at desc";
    $queryLvl4 = mysqli_query($con, $sqlLvl4);

    while( $fetchLvl4 = mysqli_fetch_array($queryLvl4) ){
      //$recLvl4 = mysqli_num_rows($queryLvl4);
      $recLvl4++;
      $userid_lv4 = $fetchLvl4['tg_id'];
      $name_lv4 = $fetchLvl4['username'];
      $acct_usern_lvl4 = $fetchLvl4['ref_id'];
        $user_acctsusr4[] = $acct_usern_lvl4;
        //total active invest
        $sqlCommL4 = "select sum(amt_usd) as total_usd from robots where tg_id = '$userid_lv4' and profit_usd < amt_usd group by user_id";
        $queryCommL4 = mysqli_query($con, $sqlCommL4);
            while( $fetchCom4 = mysqli_fetch_array($queryCommL4) ){
              $amtUsd_lv4 += $fetchCom4['total_usd'];
            }
    }
  }

  //Team Level 5
  foreach( $user_acctsusr4 as $key => $user_accts5 ){
    $sqlLvl5 = "select tg_id,username,ref_id from users where sponsor_id = '$user_accts5' order by created_at desc";
    $queryLvl5 = mysqli_query($con, $sqlLvl5);

    while( $fetchLvl5 = mysqli_fetch_array($queryLvl5) ){
      //$recLvl5 = mysqli_num_rows($queryLvl5);
      $recLvl5++;
      $userid_lv5 = $fetchLvl5['tg_id'];
      $name_lv5 = $fetchLvl5['username'];
      //echo "<p>".$name_lv5."</p>";
      $acct_usern_lvl5 = $fetchLvl5['ref_id'];
        $user_acctsusr5[] = $acct_usern_lvl5;
        //total active invest
        $sqlCommL5 = "select sum(amt_usd) as total_usd from robots where tg_id = '$userid_lv5' and profit_usd < amt_usd group by user_id";
        $queryCommL5 = mysqli_query($con, $sqlCommL5);
            while( $fetchCom5 = mysqli_fetch_array($queryCommL5) ){
              $amtUsd_lv5 += $fetchCom5['total_usd'];
            }
    }
  }

  //Team Level 6
  foreach( $user_acctsusr5 as $key => $user_accts6 ){
    $sqlLvl6 = "select tg_id,username,ref_id from users where sponsor_id = '$user_accts6' order by created_at desc";
    $queryLvl6 = mysqli_query($con, $sqlLvl6);

    while( $fetchLvl6 = mysqli_fetch_array($queryLvl6) ){
      //$recLvl6 = mysqli_num_rows($queryLvl6);
      $recLvl6++;
      $userid_lv6 = $fetchLvl6['tg_id'];
      $name_lv6 = $fetchLvl6['username'];
      //echo "<p>val ".$name_lv6."</p>";
      $acct_usern_lvl6 = $fetchLvl6['ref_id'];
        $user_acctsusr6[] = $acct_usern_lvl6;
          //total active invest
          $sqlCommL6 = "select sum(amt_usd) as total_usd from robots where tg_id = '$userid_lv6' and profit_usd < amt_usd group by user_id";
          $queryCommL6 = mysqli_query($con, $sqlCommL6);
          while( $fetchCom6 = mysqli_fetch_array($queryCommL6) ){
            $amtUsd_lv6 += $fetchCom6['total_usd'];
          }
    }
  }

  //DRB / INDIRECT BREFERRAL
  $drbIncome = "";
  $userId = userInfo($con,$tg_id,"id");
  $queryDrbScd = mysqli_query($con, "select sum(po_amt) as po_amt from drbpayouts where user_id = '$userId' and `status` is null");
  //echo "<br>ins err: ".mysqli_error($con);
  $recDrbScd = mysqli_num_rows($queryDrbScd);

  while( $fetchDrbScd = mysqli_fetch_array($queryDrbScd) ){
    $drbPoAmt = $fetchDrbScd['po_amt'];
  }

  //DRB PAID AMT
  $drbPaidAmt = getPaidReferral($con,$tg_id);

  //echo "Level 1 Referrals: ".$rec."<br />";
  /*if( empty($amtUsd_lv1) ){ $amtUsd_lv1 = 0; }
  echo "total amt invested lvl 1: ".$amtUsd_lv1." USD"."<br />";*/


  //echo "Level 2 Referrals: ".$recLvl2."<br />";
  /*if( empty($amtUsd_lv2) ){ $amtUsd_lv2 = 0; }
  echo "total amt invested lvl 2: ".$amtUsd_lv2." USD"."<br />";*/


  //echo "Level 3 Referrals: ".$recLvl3."<br />";
  /*if( empty($amtUsd_lv3) ){ $amtUsd_lv3 = 0; }
  echo "total amt invested lvl 3: ".$amtUsd_lv3." USD"."<br />";*/


  //echo "Level 4 Referrals: ".$recLvl4."<br />";
  /*if( empty($amtUsd_lv4) ){ $amtUsd_lv4 = 0; }
  echo "total amt invested lvl 4: ".$amtUsd_lv4." USD"."<br />";*/


  //echo "Level 5 Referrals: ".$recLvl5."<br />";
  /*if( empty($amtUsd_lv5) ){ $amtUsd_lv5 = 0; }
  echo "total amt invested lvl 5: ".$amtUsd_lv5." USD"."<br />";*/


  //echo "Level 6 Referrals: ".$recLvl6."<br />";
  //if( empty($amtUsd_lv6) ){ $amtUsd_lv6 = 0; }

  //echo "Your Referral Income: ".$drbPoAmt;
  //echo "<br>val ".$userId;

  if( empty($rec) ){ $rec = 0; }
  if( empty($recLvl2) ){ $recLvl2 = 0; }
  if( empty($recLvl3) ){ $recLvl3 = 0; }
  if( empty($recLvl4) ){ $recLvl4 = 0; }
  if( empty($recLvl5) ){ $recLvl5 = 0; }
  if( empty($recLvl6) ){ $recLvl6 = 0; }
  if( empty($drbPoAmt) ){ $drbPoAmt = 0; }
  if( empty($drbPaidAmt) ){ $drbPaidAmt = 0; }

  $fxTeamsMsg = fxTeamMsg($rec,$recLvl2,$recLvl3,$recLvl4,$recLvl5,$recLvl6,$drbPoAmt,$drbPaidAmt);

  return $fxTeamsMsg;
}

function collectRobotProfit($con,$tg_id,$col,$robotName){
  //get profit usd
  $sqlTradeProfit = "select
	sum(t.profit_usd) as profit_usd
from robots r
left join robottrades t
on (t.portfolio_id = r.portfolio_id)
where r.tg_id = '$tg_id'
and r.robot = '$robotName'
and t.status is null";
  $queryTradeProfit = mysqli_query($con, $sqlTradeProfit);
  $fetchTp = mysqli_fetch_assoc($queryTradeProfit);
    $tradeProfit = $fetchTp[$col];

  return $tradeProfit;
}

function robotProfit($con,$tg_id,$col){
  //get profit usd
  //$sqlTradeProfit = "select r.$col from robots r left join payouts p on (r.portfolio_id = p.portfolio_id) where r.tg_id = '$tg_id' and r.robot = '$robotName' ";
  $sqlTradeProfit = "select
	sum(t.profit_usd) as profit_usd
from robots r
left join robottrades t
on (t.portfolio_id = r.portfolio_id)
where r.tg_id = '$tg_id'
and t.status is null";
  $queryTradeProfit = mysqli_query($con, $sqlTradeProfit);
  $fetchTp = mysqli_fetch_assoc($queryTradeProfit);
    $tradeProfit = $fetchTp[$col];

  return $tradeProfit;
}

function totalRobotProfit($con,$tg_id){
  //$sql_trp = "select sum(po_amt) as po_amt from payouts where tg_id = '$tg_id' and status = 'PAID'";
  $sql_trp = "select sum(t.profit_usd) as po_amt
from robots r
left join robottrades t
on (t.portfolio_id = r.portfolio_id)
where r.tg_id = '$tg_id'
and t.status = 'PAID'";
  $queryTrp = mysqli_query($con,$sql_trp);
  $rowTrp = mysqli_fetch_assoc($queryTrp);
    $totalRp = $rowTrp['po_amt'];

  return $totalRp;
}

function fx_robot($con,$tg_id){
  require_once('language.php');

  //Hired-Robot counters
  $hiredArtemysBot = countRobots($con,$tg_id,"ARTEMYS");
  $hiredArthurBot = countRobots($con,$tg_id,"ARTHUR");
  $hiredDartanianBot = countRobots($con,$tg_id,"DARTANIAN");

  $artemysProfit = collectRobotProfit($con,$tg_id,"profit_usd","ARTEMYS");
    if(empty($artemysProfit)){$artemysProfit = 0;}else{$artemysProfit = $artemysProfit;}
  $arthurProfit = collectRobotProfit($con,$tg_id,"profit_usd","ARTHUR");
    if(empty($arthurProfit)){$arthurProfit = 0;}else{$arthurProfit = $arthurProfit;}
  $dartanianProfit = collectRobotProfit($con,$tg_id,"profit_usd","DARTANIAN");
    if(empty($dartanianProfit)){$dartanianProfit = 0;}else{$dartanianProfit = $dartanianProfit;}

  $roboProfit = robotProfit($con,$tg_id,"profit_usd");
    if(empty($roboProfit)){$roboProfit = 0;}else{$roboProfit = $roboProfit;}

  $tradeProfitTotal = $roboProfit;
    if(empty($tradeProfitTotal)){$tradeProfitTotal = 0;}else{$tradeProfitTotal = number_format($tradeProfitTotal,2);}

  $totalTrp = totalRobotProfit($con,$tg_id);
    if(empty($totalTrp)){$totalTrp = 0;}else{$totalTrp = number_format($totalTrp,2);}

  $fxRobotMsg = fxRobots($hiredArtemysBot,$hiredArthurBot,$hiredDartanianBot,$artemysProfit,$arthurProfit,$dartanianProfit,$tradeProfitTotal,$totalTrp);

  return $fxRobotMsg;
}

function collectTradeProfit($con,$tg_id){
  $sqlCollection = "select date_format(po_sched,'%M %d, %Y') as po_sched from payouts where tg_id = '$tg_id' and `status` is null group by po_sched";
  $collectionQuery = mysqli_query($con, $sqlCollection);
    $fetchCollect = mysqli_fetch_assoc($collectionQuery);
      $profitCollectionSched = $fetchCollect['po_sched'];

  $collectProfitSched = $profitCollectionSched;

  return $collectProfitSched;
}

function insertCommand($con,$tg_id,$command){
  //check first if command exist
  $sqlCheckUsr = "select command from usercommands where tg_id = '$tg_id'";
  $queryCheckUsr = mysqli_query($con, $sqlCheckUsr);
  $isEMptyCom = mysqli_num_rows($queryCheckUsr);

  if($isEMptyCom > 0){
    $sqlComm = "update usercommands set command = '$command' where tg_id = '$tg_id'";
  }else{
    $sqlComm = "insert into usercommands (tg_id,command) values ('".$tg_id."','".$command."')";
  }

  $queryComm = mysqli_query($con, $sqlComm);
}

function getPayoutAddress($con,$tg_id){
  $sqlPa = "select payout_address from users where tg_id = '$tg_id'";
  $queryPa = mysqli_query($con, $sqlPa);
  $fetchPa = mysqli_fetch_assoc($queryPa);
    $payoutAddress = $fetchPa['payout_address'];

  return $payoutAddress;
}

function getLastCommand($con, $tg_id){
  $sqlComm = "select command from usercommands where tg_id = '$tg_id'";
  $queryComm = mysqli_query($con,$sqlComm);
  $fetchComm = mysqli_fetch_assoc($queryComm);
    $lastCommand = $fetchComm['command'];

  return $lastCommand;
}

function savePayoutAddress($con,$tg_id,$payout_address){
  $sqlSpa = "update users set payout_address = '$payout_address' where tg_id = '$tg_id'";
  $querySpa = mysqli_query($con, $sqlSpa);
}

function removLastComm($con,$tg_id){
  $sqlRlc = "delete from usercommands where tg_id = '$tg_id'";
  $queryRlc = mysqli_query($con, $sqlRlc);
}

function getReferralLink($con, $tg_id){
  $sqlGrl = "select ref_id from users where tg_id = '$tg_id'";
  $queryGrl = mysqli_query($con, $sqlGrl);
    $fetchGrl = mysqli_fetch_assoc($queryGrl);
      $referralId = $fetchGrl['ref_id'];

    return $referralId;
}

function getPaidReferral($con,$tg_id){
  $userId = userInfo($con,$tg_id,"id");
  $queryDrbPd = mysqli_query($con, "select sum(po_amt) as pd_amt from drbpayouts where user_id = '$userId' and `status` = 'PAID'");
  $fetchDrbPd = mysqli_fetch_assoc($queryDrbPd);
    $drbPaid = $fetchDrbPd['pd_amt'];

  return $drbPaid;
}

?>
