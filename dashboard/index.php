<?php
session_start();
include('../php-includes/check-login.php');
require('../php-includes/connect.php');
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Dashboard - FXT v2</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="../assets/images/favicon.ico">

        <link rel="stylesheet" href="../plugins/morris/morris.css">

        <!-- App css -->
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />

        <script src="../assets/js/modernizr.min.js"></script>

    </head>

    <body>

        <!-- Navigation Bar-->
        <?php include('../php-includes/nav_bar.php'); ?>
        <!-- End Navigation Bar-->


        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="btn-group pull-right">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">FXT v2</a></li>
                                    <li class="breadcrumb-item active">Dashboard</li>
                                </ol>
                            </div>
                            <!-- <h4 class="page-title">Welcome !</h4> -->
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->

                <div class="row">

                    <div class="col-md-3">
                        <div>
                          <!-- Left Box 1-->
                          <?php include('../php-includes/left-box1.php'); ?>
                          <!-- Left Box 1-->
                        </div>
                    </div>

                    <div class="col-md-3">
                      <div class="portlet"><!-- /primary heading -->
                        <div class="portlet-heading">
                          <p>
                            <span style="font-size: 120%; color: #212121;">Total Capital</span><br />
                            <span class="text-dark" style="font-size: 220%;"><i class="mdi mdi-coin"></i>700 USD</span><br />
                            <span id="dispwalbalusd" style="font-size: 100%;" class="text-warning"></span>
                            <input type="hidden" id="walbal" value="">
                          </p>
                          <!-- <p>
                            <span style="font-size: 100%;" class="text-success">0.5612002000 BTC</span>
                          </p> -->
                        </div>
                      </div>

                    </div><!-- /col-md-3 -->

                    <div class="col-md-3">
                      <div class="portlet">
                          <div class="portlet-heading">
                            <p>
                              <span style="font-size: 120%; color: #212121;">Total Profit</span><br />
                              <span class="text-dark" style="font-size: 220%;"><i class="mdi mdi-coin"></i></span>
                              <span class="text-primary" style="font-size: 220%;">0.00 USD</span><br />
                            </p>
                            <!--<p>
                              <span style="font-size: 100%;;" class="text-warning">0.05005641 BTC</span>
                            </p>-->
                        </div>
                      </div>

                    </div>

                    <div class="col-md-3">
                      <div class="portlet">
                          <div class="portlet-heading">
                            <p>
                              <span style="font-size: 120%; color: #212121;">Active Portfolios</span><br />
                              <span class="text-dark" style="font-size: 220%;"><i class="mdi mdi-book-open"></i></span>
                              <span class="text-success" style="font-size: 220%;">3</span><br />
                            </p>
                            <!--<p>
                              <span style="font-size: 100%;" class="text-dark">0.05005641 BTC</span>
                            </p>-->
                        </div>
                      </div>

                    </div>
                </div>

                <div class="row">
                    <!-- /filler -->
                      <div class="col-md-3"><p>&nbsp;</p></div>
                    <!-- /filler -->
                    <div class="col-md-7">
                      <div class="card-box">
                        <h4 class="text-dark  header-title m-t-0">Referral Link</h4>
                        <div class="form-group">
                            <label for="refLink"><i class="mdi mdi-link-variant align-middle" style="font-size: 100%;"></i>&nbsp;Copy referral link</label>
                            <input type="text" class="form-control" id="refLink" name="refLink" value="http://localhost/register/?sp=Hu6aax" readonly>
                        </div>
                      </div>
                    </div>
                </div>

                <div class="row">
                  <!-- /filler -->
                    <div class="col-md-3"><p>&nbsp;</p></div>
                  <!-- /filler -->

                  <div class="col-md-7">
                      <div>
                        <div class="card-box">
                            <h4 class="text-dark  header-title m-t-0 m-b-30">Total Revenue</h4>

                            <div class="widget-chart text-center">
                                <div id="sparkline1"><canvas width="308" height="200" style="display: inline-block; width: 308.328px; height: 200px; vertical-align: top;"></canvas></div>
                            </div>
                        </div>
                      </div>
                  </div>

                  <!-- /filler -->
                    <div class="col-md-1"><p>&nbsp;</p></div>
                  <!-- /filler -->
                </div>

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->


        <!-- Footer -->
        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        2019 © FXT v2
                    </div>
                </div>
            </div>
        </footer>
        <!-- End Footer -->


        <!-- jQuery  -->
        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/popper.min.js"></script><!-- Popper for Bootstrap --><!-- Tether for Bootstrap -->
        <script src="../assets/js/bootstrap.min.js"></script>
        <script src="../assets/js/waves.js"></script>
        <script src="../assets/js/jquery.slimscroll.js"></script>
        <script src="../assets/js/jquery.scrollTo.min.js"></script>

        <!-- Counter Up  -->
        <script src="../plugins/waypoints/lib/jquery.waypoints.min.js"></script>
        <script src="../plugins/counterup/jquery.counterup.min.js"></script>

        <!--Morris Chart-->
        <script src="../plugins/morris/morris.min.js"></script>
        <script src="../plugins/raphael/raphael-min.js"></script>

        <!-- Page js  -->
        <script src="../assets/pages/jquery.dashboard.js"></script>

        <!-- Custom main Js -->
        <script src="../assets/js/jquery.core.js"></script>
        <script src="../assets/js/jquery.app.js"></script>


        <script type="text/javascript">
            jQuery(document).ready(function($) {
                $('.counter').counterUp({
                    delay: 100,
                    time: 1200
                });
            });
        </script>

    </body>
</html>
