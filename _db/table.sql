CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sponsor_id` varchar(100) DEFAULT NULL,
  `ref_id` varchar(6) DEFAULT NULL,
  `first_name` varchar(150) DEFAULT NULL,
  `last_name` varchar(150) DEFAULT NULL,
  `middle_name` varchar(150) DEFAULT NULL,
  `username` varchar(200) DEFAULT NULL,
  `deposit_address` varchar(50) NOT NULL,
  `payout_address` varchar(50) DEFAULT NULL,
  `balance` int(12) NOT NULL DEFAULT '0',
  `token` varchar(200) DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `users_idx` (`sponsor_id`,`ref_id`,`username`,`deposit_address`,`payout_address`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
