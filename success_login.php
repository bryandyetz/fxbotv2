<?php
session_start();
include('php-includes/check-login.php');
require('php-includes/connect.php');
?>
<html>
  <head>
    <link rel="shortcut icon" href="assets/images/favicon.ico">

    <!-- Sweet Alert css -->
    <link href="plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css" />

    <!-- App css -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/style.css" rel="stylesheet" type="text/css" />

    <script src="assets/js/modernizr.min.js"></script>
    <script src="assets/js/jquery.min.js"></script>
    <script type="text/javascript">
      $(document).ready(function() {
        swal({
          title: "Login Success!",
          text: "",
          type: "success",
          showCancelButton: false,
          confirmButtonColor: '#F9CD48'
        },
        function(isConfirm) {
            if (isConfirm) {
                window.location.assign("dashboard");
            }
        });
      });
    </script>
  </head>
  <body>
    <!-- jQuery  -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/popper.min.js"></script><!-- Popper for Bootstrap --><!-- Tether for Bootstrap -->
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/waves.js"></script>
    <script src="assets/js/jquery.slimscroll.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>

    <!-- Sweet Alert js -->
    <script src="plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
    <script src="assets/pages/jquery.sweet-alert.init.js"></script>

    <!-- App js -->
    <script src="assets/js/jquery.core.js"></script>
    <script src="assets/js/jquery.app.js"></script>
  </body>
</html>
