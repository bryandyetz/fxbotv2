<?php
session_start();
$admin_user_id = $_SESSION['userid'];
?>
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.html">FOREX TRADER ROBOTS</a>
    </div>
    <!-- /.navbar-header -->
    <ul class="nav navbar-top-links navbar-right">

        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-message">
                <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                </li>
                <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                </li>
                <li class="divider"></li>
                <li><a href="../logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                </li>
            </ul>
            <!-- /.dropdown-user -->
        </li>
        <!-- /.dropdown -->
    </ul>
    <!-- /.navbar-top-links -->

    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                <li>
                    <a href="dashboard.php"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                </li>
              <?php if( $admin_user_id == "admin" ){ ?>
                <li>
                    <a href="robot_trades.php"><i class="glyphicon glyphicon-stats fa-fw"></i> Robot Weekly Trades</a>
                </li>
                <li>
                    <a href="submit_autotrade.php"><i class="fa fa-money fa-fw"></i> Robot Auto Trades</a>
                </li>
                <li>
                    <a href="robot_payout.php"><i class="fa fa-money fa-fw"></i> Robot Payouts Updater</a>
                </li>
                <li>
                    <a href="collect_btc.php"><i class="fa fa-money fa-fw"></i> Collect BTC</a>
                </li>
              <?php } ?>
                <!-- <li>
                    <a href="create_blk.php"><i class="fa fa-adjust fa-fw"></i>  Generate Blocks</a>
                </li> -->
              <?php if( $admin_user_id == "admin" || $admin_user_id == "admin_rich" ){ ?>
                <li>
                    <a href="send_btc.php"><i class="fa fa-bitcoin fa-fw"></i>  Send BTC</a>
                </li>
              <?php } ?>
                <!-- <li>
                    <a href="send_link.php"><i class="fa fa-adjust fa-fw"></i>  Send Referral Link</a>
                </li> -->
                <!-- <li>
                    <a href="calltask.php"><i class="fa fa-adjust fa-fw"></i> Accomplishments</a>
                </li> -->
            </ul>
        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
</nav>
